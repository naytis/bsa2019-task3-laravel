<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetAllProductsAction
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(): GetAllProductsResponse
    {
        $products = $this->productRepository->findAll();

        return new GetAllProductsResponse($products);
    }
}