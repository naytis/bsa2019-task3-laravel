<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(): GetCheapestProductsResponse
    {
        $products = $this->productRepository->getCheapest();

        return new GetCheapestProductsResponse($products);
    }
}