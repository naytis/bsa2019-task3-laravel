<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        $product = $this->productRepository->getMostPopular();

        return new GetMostPopularProductResponse($product);
    }
}