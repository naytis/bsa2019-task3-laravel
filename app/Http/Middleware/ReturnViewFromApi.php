<?php

namespace App\Http\Middleware;

use Closure;

class ReturnViewFromApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $products = $response->getData(true);

        return response()->view('cheap_products', ['products' => $products]);
    }
}
