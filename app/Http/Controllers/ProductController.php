<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;

class ProductController extends Controller
{
    private $presenter;
    private $getAllProductsAction;
    private $getMostPopularProductAction;
    private $getCheapestProductsAction;

    public function __construct(
        ProductArrayPresenter $presenter,
        GetAllProductsAction $getAllProductsAction,
        GetMostPopularProductAction $getMostPopularProductAction,
        GetCheapestProductsAction $getCheapestProductsAction
    ) {
        $this->presenter = $presenter;
        $this->getAllProductsAction = $getAllProductsAction;
        $this->getMostPopularProductAction = $getMostPopularProductAction;
        $this->getCheapestProductsAction = $getCheapestProductsAction;
    }

    public function getAllProducts()
    {
        $products = $this->getAllProductsAction->execute()->getProducts();

        return $this->presenter->presentCollection($products);
    }

    public function getMostPopularProduct()
    {
        $product = $this->getMostPopularProductAction->execute()->getProduct();

        return $this->presenter->present($product);
    }

    public function getCheapestProducts()
    {
        $products = $this->getCheapestProductsAction->execute()->getProducts();

        return $this->presenter->presentCollection($products);
    }
}