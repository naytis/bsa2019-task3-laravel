<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;

class ProductRepository implements ProductRepositoryInterface
{
    private $products;

	public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function findAll(): array
    {
        return $this->products;
    }

    public function getMostPopular(): Product
    {
        return array_reduce(
            $this->products,
            function (Product $productA, Product $productB) {
                return $productA->getRating() > $productB->getRating() ? $productA : $productB;
            },
            $this->products[0]
        );
    }

    public function getCheapest(): array
    {
        $products = $this->products;

        usort($products, function (Product $productA, Product $productB) {
            return $productA->getPrice() <=> $productB->getPrice();
        });

        return array_slice($products, 0 , 3);
    }
}
