<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));

        for ($products = [], $i = 1; $i <= 100; $i++) {
            $products[] = new Product(
                $i,
                $faker->unique()->productName(),
                $faker->randomFloat(2, 100, 1000),
                $faker->imageUrl(),
                $faker->randomFloat(1, 1, 5)
            );
        }

        return $products;
    }
}