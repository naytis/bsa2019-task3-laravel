<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/0d9d5dab9a.js"></script>
    <style>
        .card {
            transition: .3s;
        }

        .card:hover {
            transform: scale(1.01);
            box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
        }

        h1, h5 {
            font-weight: 300;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1 class="my-4">Cheapest products</h1>
        <div class="card-deck">
            @foreach($products as $product)
                <div class="card" style="width: 18rem;">
                    <img src="{{ $product['img'] }}" class="card-img-top" alt="...">
                    <div class="card-body d-flex flex-column align-items-center">
                        <h5 class="card-title">

                            ${{ $product['price'] }}
                        </h5>
                        <p>{{ $product['name'] }}</p>
                        <div class="rating">
                            @foreach(range(1,5) as $i)
                                @if($product['rating'] > 0)
                                    @if($product['rating'] > 0.5)
                                        <i class="fas fa-star"></i>
                                    @else
                                        <i class="fas fa-star-half-alt"></i>
                                    @endif
                                @else
                                    <i class="far fa-star"></i>
                                @endif
                                @php $product['rating']--; @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</body>
</html>